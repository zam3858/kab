<?php

namespace App\Http\Controllers;

use App\Model\State;
use Illuminate\Http\Request;

class StateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $states = State::get();

        return view('states.index', compact('states'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('states.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $state = new State();

        $state->code = request()->code;
        $state->name = request()->name;

        $state->save();

        return redirect('/states');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function show( $id )
    {
        $state = State::findOrFail($id);
        return view('states.show', compact('state'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $state = State::findOrFail($id);
        return view('states.edit', compact('state'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
         $validatedData = request()->validate([
                'code' => 'required',
                'name' => 'required',
            ]
        );

        $state = State::findOrFail($id);

        $state->code = request()->code;
        $state->name = request()->name;

        $state->save();

        return redirect('/states');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id )
    {
        $state = State::findOrFail($id);

        $state->delete();

        return redirect('/states');

    }
}
