@extends('layouts.app')

@section('content')

<form action="{{ route('states.store') }}" method="POST">
    @csrf
    <div>
        <div class="form-group">
            <label for="">Code</label>
            <input type="text" class="form-control" name="code">
        </div>
        <div class="form-group">
            <label for="">Name</label>
            <input type="text" class="form-control" name="name">
        </div>
        <div class="form-group">
            <button>Submit</button>
        </div>
    </div>
</form>

@endsection