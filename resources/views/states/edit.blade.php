@extends('layouts.app')

@section('content')

<form action="{{ route('states.update', $state->id) }}" method="POST">
    @csrf
    @method('PUT')
    <div>
        <div class="form-group">
            <label for="">Code</label>
            <input type="text" class="form-control" name="code"
            	value="{{ old('code',$state->code) }}"
            >
            @error('code')
			    <div class="alert alert-danger">{{ $message }}</div>
			@enderror
        </div>
        <div class="form-group">
            <label for="">Name</label>
            <input type="text" class="form-control" name="name"
            	value="{{ old('name',$state->name) }}"
            >
            @error('name')
            	<div>
            		{{ $message }}
            	</div>
            @enderror
        </div>
        <div class="form-group">
            <button>Submit</button>
        </div>
    </div>
</form>

@endsection