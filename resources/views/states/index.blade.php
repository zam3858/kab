@extends('layouts.app')

@section('content')
<table class="table table-striped">
    <tr>
        <th>
            Code
        </th>
        <th>
            Name
        </th>
        <th></th>
    </tr>
    @foreach($states as $state)
        <tr>
            <td>
                {{ $state->code }}
            </td>
            <td>
                {{ $state->name }}
            </td>
            <td>
                <a href="{{ route('states.show', $state->id) }}" 
                    class="btn btn-primary"
                    >View</a>
                <a href="{{ route('states.edit', $state->id) }}" 
                    class="btn btn-primary"
                    >Edit</a>

                <form method='POST' action="{{ route('states.destroy', $state->id) }}" >
                    @csrf
                    @method('delete')  
                    <button class="btn btn-warning"> Delete </button>
                </form>
            </td>
        </tr>
    @endforeach
</table>
@endsection