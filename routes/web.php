<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// http://localhost:8000/states/1
// Route::get('/states', 'StateController@index')->name('states.index');
// Route::get('/states/create', 'StateController@create')
//                                 ->name('states.create');
// Route::post('/states', 'StateController@store')
//                                 ->name('states.store');
// Route::get('/states/{id}','StateController@show')
// 								->name('states.show');
// Route::get('/states/{id}/edit','StateController@edit')
// 								->name('states.edit');
// Route::put('/states/{id}','StateController@update')
// 								->name('states.update');
// Route::delete('/states/{id}','StateController@destroy')
// 								->name('states.destroy');

//Route::get('/states/search', 'StateController@search');
Route::resource('/states', 'StateController');
